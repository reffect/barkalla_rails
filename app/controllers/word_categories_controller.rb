class WordCategoriesController < ApplicationController
  before_action :set_word_category, only: [:show, :edit, :update, :destroy]

  # GET /word_categories
  def index
    @word_categories = WordCategory.all
  end

  # GET /word_categories/1
  def show
  end

  # GET /word_categories/new
  def new
    @word_category = WordCategory.new
  end

  # GET /word_categories/1/edit
  def edit
  end

  # POST /word_categories
  def create
    @word_category = WordCategory.new(word_category_params)

    if @word_category.save
      redirect_to @word_category, notice: 'Word category was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /word_categories/1
  def update
    if @word_category.update(word_category_params)
      redirect_to @word_category, notice: 'Word category was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /word_categories/1
  def destroy
    @word_category.destroy
    redirect_to word_categories_url, notice: 'Word category was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_word_category
      @word_category = WordCategory.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def word_category_params
      params.require(:word_category).permit(:name, :body)
    end
end
