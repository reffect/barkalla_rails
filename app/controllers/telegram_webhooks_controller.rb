class TelegramWebhooksController < Telegram::Bot::UpdatesController
  include Telegram::Bot::UpdatesController::MessageContext
  use_session!

  ADMIN_ID = [1291549]
  SESSION_WORD = :word
  SESSION_EDIT = :edit

  def start
    user = User.find_or_create_by(telegram_id: from['id'].to_i)
    user.last_name = from['last_name']
    user.first_name = from['first_name']
    user.username = from['username']
    user.role = :user
    user.save

    respond_with :message,
                 text: "<b>#{from['first_name']} #{from['last_name']}</b>, добро пожаловать! \nЭто телеграм бот кубачинского словаря",
                 parse_mode: 'HTML'
  end

  def setadmin(username)
    return unless this_admin
    user = User.find_by_username(username)

    if user
      user.role = :admin
      user.save
      respond_with :message,text: "<b>#{username}</b> теперь админ! ", parse_mode: 'HTML'
    else
      respond_with :message,text: "<b>#{username}</b> Не зарегистрирован в системе!", parse_mode: 'HTML'
    end
  end

  def banuser(username)
    return unless this_admin
    user = User.find_by_username(username)

    if user
      user.role = :ban
      user.save
      respond_with :message,text: "<b>#{username}</b> теперь админ! ", parse_mode: 'HTML'
    else
      respond_with :message,text: "<b>#{username}</b> Не зарегистрирован в системе!", parse_mode: 'HTML'
    end
  end

  def unsetadmin(username)
    return unless this_admin
    user = User.find_by_username(username)

    if user
      user.role = :user
      user.save
      respond_with :message,text: "<b>#{username}</b> теперь админ! ", parse_mode: 'HTML'
    else
      respond_with :message,text: "<b>#{username}</b> Не зарегистрирован в системе!", parse_mode: 'HTML'
    end
  end

  def help
    respond_with :message,
                 text: "Просто отправьте слово, чтоб его перевести!",
                 parse_mode: 'HTML'
  end


  def edit(*args)
    unless this_admin
      return respond_with :message, text: "Вы не можете модерировать слова... обратитесь к @rabadan731 для получения прав"
    end

    word_text = args.join(' ')
    if word_text.present?
      find_word = Word.find_word(word_text)
      if find_word.present?
        return editnew1(find_word.id)
      else
        return respond_with :message,
                     text: "Слово \"#{word_text}\" в словаре не найдено! проверьте правильность ввода",
                     parse_mode: 'HTML'
      end
    else
      return respond_with :message,
                   text: "Для редактирования слова, отправьте <b>\"/edit слово\"</b>",
                   parse_mode: 'HTML'
    end
  end

  def text(*args)
    puts '--------------------------'
    puts args.first(50).inspect
    puts '--------------------------'
    rus = 0
    kub = 0
    output = []
    args.first(50).each do |word_text|
      word_text.downcase!
      find_word = Word.find_word(word_text)
      if find_word.nil?
        output << word_text
      else
        if word_text == find_word.kub
          kub += 1
        else
          rus += 1
        end
        output << find_word
      end
    end

    if kub+rus > 0
      output_text = []
      output_text << "<i>#{kub >= rus ? 'Первод с кубачинсокого на русский' : 'Первод с русского на кубачинский'}</i>\n"
      output_text << output.map do |item|
        if item.class == Word
          "<b>#{kub >= rus ? item.rus : item.kub}</b>"
        else
          "<i>#{item}</i>"
        end
      end

      respond_with :message, text: output_text.join(' '), parse_mode: 'HTML'
    else
      respond_with :message, text: "перевод данных слов не найден...", parse_mode: 'HTML'
    end
  end

  def message(message)
    word_text = message['text'].downcase
    word = Word.find_word(word_text)

    if word.nil?
      if Word.in_moderate?(word_text)
        respond_with :message, text: "Слово '<b>#{word_text}</b>' находится на модерации", parse_mode: 'HTML'
      else
        ask_create_word(word_text, message['message_id'])
      end
    else
      if word.kub == word_text
        respond_with :message,
                     text: "<i>Первод с кубачинсокого на русский</i>\n<b>#{word.rus}</b>",
                     parse_mode: 'HTML'
      else
        respond_with :message,
                     text: "<i>Первод с русского на кубачинский</i>\n<b>#{word.kub}</b>",
                     parse_mode: 'HTML'
      end
    end
  end

  def word(*args)
    word_text = args.join(' ')
    word_text = word_text.downcase
    word = Word.find_word(word_text)
    if word.nil?
      if Word.in_moderate?(word_text)
        respond_with :message,
                     text: "Слово '<b>#{word_text}</b>' находится на модерации", parse_mode: 'HTML'
      else
        respond_with :message, text: " <b>#{word_text}</b> Слово не найдено словаре (", parse_mode: 'HTML'
      end
    else
      if word.kub == word_text
        respond_with :message,
                     text: "<i>Первод с кубачинсокого на русский</i>\n<b>#{word.rus}</b>",
                     parse_mode: 'HTML'
      else
        respond_with :message,
                     text: "<i>Первод с русского на кубачинский</i>\n<b>#{word.kub}</b>",
                     parse_mode: 'HTML'
      end
    end
  end

  def callback_query(data)
    params = parse_callback(data)

    case params[:action]
      when :new
        word_text = find_word_in_session(params[:message_id].to_i)
        if create_word(word_text, params[:param])
          return edit_message :text,
             text: "Спасибо! Cлово <b>\"#{word_text}\"</b> добавлено в список на добавления в словарь!",
             parse_mode: 'HTML'
        else
          return edit_message :text, text: "Ошибка добавления слова.. обратитесь к администратору.."
        end
      when :save
        save_word(params[:message_id])
        edit_message :text, text: "Слово успешно сохранено"
        moderation
      when :delete
        delete_word(params[:message_id])
        edit_message :text, text: "Слово успешно удалено"
        moderation
      when :listedit
        return moderation(params[:message_id])
      when :editnew
        return editnew2(nil, word_id: params[:message_id], param: params[:param])
      when :cancel
        text_msg = if params[:param] == 'add'
                     word_text = find_word_in_session(params[:message_id])
                     "<b>#{word_text}</b> \nСлово не найдено словаре ("
                   elsif params[:param] == 'edit'
                     "Вы вышли из режима модерации слов"
                   else
                     "Отмена операции.."
                   end
        edit_message :text, text: "#{text_msg}" , parse_mode: 'HTML'
      else
        respond_with :message, text: "Неизвестная ошибка 2.. обратитесь к администратору.."
    end
    delete_in_session(params[:message_id])
  end

  def ask_create_word(word_text, message_id = nil)
    session[SESSION_WORD] = {} if session[SESSION_WORD].nil?
    session[SESSION_WORD][message_id] = word_text

    respond_with :message,
                 text: " <b>#{word_text}</b>
Слово не найдено словаре (
Добавить слово в словарь, как? ",
                 parse_mode: 'HTML',
                 reply_markup: {
                     inline_keyboard: [
                         [
                             {text: 'Русское', callback_data: "new_rus_#{message_id}"},
                             {text: 'Кубачинское', callback_data: "new_kub_#{message_id}"}
                         ],
                         [{text: 'Не добавлять в словарь', callback_data: "cancel_add_#{message_id}"}]
                     ],
                 }
  end

  def moderation(data = 0)

    unless this_admin
      return respond_with :message, text: "Вы не можете модерировать слова... обратитесь к @rabadan731 для получения прав"
    end

    words = Word.where(status: :moderated).order(id: :desc).limit(1).offset(data).all
    if words.length < 1
      return respond_with :message, text: "нет слов на модерации.."
    end
    words.each_with_index do |item, key|
      editnew1(item.id, data)
    end
  end

  # первый вопрос
  def editnew1(word_id, offcet = 0)
    item = Word.find(word_id)

    output = "<b>Редактирование слова №#{item.id}</b> \n----------\n"
    output += "На русском языке:\n"
    output += "<b>#{item.rus}</b>\n"
    output += "На кубачинском:\n"
    output += " <b>#{item.kub}</b>\n"
    output += "-------------------\n"
    output += "Изменить слово:\n"

    reply_markup = {
        inline_keyboard: [
            [
                {text: 'Русское', callback_data: "editnew_rus_#{item.id}"},
                {text: 'Кубачинское', callback_data: "editnew_kub_#{item.id}"}
            ],
            [
                {text: 'Следующее слово', callback_data: "listedit_next_#{offcet.to_i+1}"}
            ],
            [
                {text: 'Сохранить', callback_data: "save_word_#{item.id}"},
                {text: 'Отмена', callback_data: "cancel_edit_#{item.id}"},
                {text: 'Удалить', callback_data: "delete_new_#{item.id}"},
            ]
        ],
    }

    if offcet.to_i > 0
      edit_message :text, text: "#{output}", parse_mode: 'HTML', reply_markup: reply_markup
    else
      respond_with :message, text: "#{output}", parse_mode: 'HTML', reply_markup: reply_markup
    end
  end

  # замена русского текста
  def editnew2(value = nil, *args)
    if value
      value = value.downcase

      if session[SESSION_EDIT][:word_id].present? && session[SESSION_EDIT][:param].present?
        word_id = session[SESSION_EDIT][:word_id]
        param = session[SESSION_EDIT][:param]
        item = Word.find(word_id)

        if param == 'kub'
          old = item.kub
          item.kub = value
          mes = "Значение перевода слова \"<b>#{item.rus}</b>\" было изменно с \"<b>#{old}</b>\" на \"<b>#{item.kub}</b>\""
        else
          old = item.rus
          item.rus = value
          mes = "Значение перевода слова \"<b>#{item.kub}</b>\" было изменно с \"<b>#{old}</b>\" на \"<b>#{item.rus}</b>\""
        end
        item.save

        respond_with :message, text: "#{mes}", parse_mode: 'HTML'
        editnew1(item.id)
      end
    else
      if args.first[:word_id].present? && args.first[:param]
        word_id = args.first[:word_id]
        param = args.first[:param]
        item = Word.find(word_id)

        message = "<b>Редактирование слова №#{item.id}</b> \n----------\n"

        save_context :editnew2

        if param == 'kub'
          session[SESSION_EDIT] = {word_id: word_id, param: param}
          message += "Введите перевод слова <b>'#{item.rus}'</b> на кубачинский язык. Текущее значение: <b>'#{item.kub}'</b>"
        else
          session[SESSION_EDIT] = {word_id: word_id, param: 'rus'}
          message += "Введите перевод слова <b>'#{item.kub}'</b> на русский язык. Текущее значение: <b>'#{item.rus}'</b>"
        end

        edit_message :text, text: "#{message}", parse_mode: 'HTML'
      else
        respond_with :message, text: "Ошибка редактирования..", parse_mode: 'HTML'
      end
    end
  end




  private

  def parse_callback(data)
    callback_data = data.split('_')
    if callback_data.length != 3
      return edit_message :text, text: "Неизвестная ошибка 1.. обратитесь к администратору.."
    end

    {
      action: callback_data[0].to_sym,
      param: callback_data[1],
      message_id: callback_data[2]
    }
  end

  def delete_in_session(message_id)
    if session[SESSION_WORD].present?
      if session[SESSION_WORD][message_id].present?
        return session[SESSION_WORD].delete(message_id)
      end
    end
  end

  def find_word_in_session(message_id)
    message_id = message_id

    if session[SESSION_WORD].present?
      if session[SESSION_WORD][message_id].present?
        return session[SESSION_WORD][message_id]
      end
    end

    nil
  end

  def delete_word(word_id)
    word = find_word(word_id)
    word.destroy
  end

  def save_word(word_id)
    word = find_word(word_id)
    word.status = :published
    word.save
  end

  def find_word(word_id)
    Word.find(word_id)
  end

  def create_word(word_text, param)
    if word_text.nil?
      return nil
    end
    word_text = word_text.downcase
    word = Word.find_by("#{param} = ?", word_text)
    if word.nil?
      word = Word.new
      word.user_id = from['id']
      word.status = :moderated

      if param == 'kub'
        word.kub = word_text
      else
        word.rus = word_text
      end

      return word.save
    end
  end

  def this_admin
    return true if ADMIN_ID.include?(from['id'])
    user = User.find_by(telegram_id: from['id'])

    user.admin?
  end

end
