class Word < ApplicationRecord
  belongs_to :word_category, optional: true

  STATUS_LIST = [:published, :moderated, :blocked]

  enum status: STATUS_LIST

  def self.find_word(word)
    word.downcase!
    Word.where(status: :published).where('"kub" = ? OR "rus" = ?', word, word).first
  end

  def self.in_moderate?(word)
    word.downcase!
    Word.where(status: :moderated).where('"kub" = ? OR "rus" = ?', word, word).count > 0
  end
end
