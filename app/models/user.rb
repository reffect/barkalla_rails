class User < ApplicationRecord
  enum role: [:user, :ban, :moderation, :admin]
end
