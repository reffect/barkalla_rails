Rails.application.routes.draw do
  resources :word_news
  resources :word_fixes
  resources :words
  resources :word_categories
  telegram_webhook TelegramWebhooksController
end
