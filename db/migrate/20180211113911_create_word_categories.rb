class CreateWordCategories < ActiveRecord::Migration[5.1]
  def change
    create_table :word_categories do |t|
      t.string :name
      t.text :body

      t.timestamps
    end
  end
end
