class CreateWords < ActiveRecord::Migration[5.1]
  def change
    create_table :words do |t|
      t.string :rus
      t.string :kub
      t.references :word_category, foreign_key: true
      t.text :body
      t.integer :status
      t.integer :user_id

      t.timestamps
    end
  end
end
