namespace :deploy do
  desc 'Run Rake Task: deploy:rake task=rake:routes'
  task :rake do
    on roles(:app) do
      within current_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, ENV['task']
        end
      end
    end
  end

  desc 'Telegram::reStart'
  task :telegram do
    on roles(:web) do
      execute 'sudo systemctl restart barkalla_bot_telegram.service'
    end
  end
  #
  # desc 'Sidekiq restart'
  # task :restart_sidekiq do
  #   on roles(:web) do
  #     execute 'sudo systemctl restart caduceus_sidekiq.service'
  #     execute 'sudo systemctl restart caduceus_sidekiq2.service'
  #   end
  # end
end
